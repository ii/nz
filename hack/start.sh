#!/bin/bash

cd $(git rev-parse --show-toplevel)

export APP_SERVE_FOLDER="${PWD}/kodata"
    APP_TEMPLATE_MAP_PATH=./template-map.yaml \
    APP_HEADER_SET_ENABLE=true \
    APP_HEADER_MAP_PATH=./template-headers.yaml \
    APP_HANDLE_GZIP=true \
    APP_METRICS_ENABLED=false \
    APP_VUEJS_HISTORY_MODE=false \
    APP_PORT=:8080 \
    BASE_URL="http://localhost:${APP_PORT//:}/"
hugo --baseURL="${BASE_URL}"
echo "URL: ${BASE_URL}"
go run .
