module gitlab.com/ii/nz

go 1.16

require (
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/prometheus/client_golang v1.12.1 // indirect
	github.com/rs/cors v1.8.2 // indirect
	gitlab.com/safesurfer/go-http-server v0.0.0-20220113202513-d8bf7c288496
	golang.org/x/sys v0.0.0-20220319134239-a9b59b0215f8 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)

replace gitlab.com/safesurfer/go-http-server => gitlab.com/BobyMCbobs/go-http-server v0.0.0-20220320083008-b8f2b4c45cad
