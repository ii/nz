# Build

# Create the container

ARG BASEIMAGE=node:16-alpine3.13
FROM $BASEIMAGE AS base
WORKDIR /app
RUN apk add --no-cache git tzdata ca-certificates && \
  adduser -D user
RUN git clone https://github.com/hakimel/reveal.js . && \
  npm i && \
  npm run build && \
  npm i reveal-multiplex && \
  mkdir -p plugin/multiplex && \
  ln node_modules/reveal-multiplex/* plugin/multiplex/

FROM scratch AS final-base
COPY --from=base /etc/group /etc/group
COPY --from=base /etc/passwd /etc/passwd
COPY --from=base /usr/lib /usr/lib
COPY --from=base /lib /lib
COPY --from=base /usr/local/bin/node /usr/local/bin/node
COPY --from=base /usr/local/lib /usr/local/lib
COPY --from=base /app /app
WORKDIR /app
USER node
ENV PATH=/usr/local/bin
ENTRYPOINT ["node", "/app/index.js"]
ENTRYPOINT ["node", "/app/node_modules/reveal-multiplex"]
