package main

import (
	common "gitlab.com/safesurfer/go-http-server/pkg/common"
	handlers "gitlab.com/safesurfer/go-http-server/pkg/handlers"
	ghs "gitlab.com/safesurfer/go-http-server/pkg/httpserver"
)

func main() {
	ghs.NewWebServer().
		SetServeFolder(common.GetEnvOrDefault("KO_DATA_PATH", "./kodata/")).
		SetHandler(&handlers.Handler{
			GzipEnabled: true,
		}).
		SetHeaderMap(map[string][]string{
			"Permissions-Policy": []string{"interest-cohort=()"},
		}).
		Listen()
}
